﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace API_REST.Controllers
{
    [Authorize]
    [RoutePrefix("api/users")]
    public class UsersController : ApiController
    {
        [HttpGet]
        public IHttpActionResult GetId(int id)
        {
            string userFake = "user-fake";
            return Ok(userFake);
        }

        [HttpGet]
        public IHttpActionResult GetAll()
        {
            string[] usersFake = new string[] { "user-1", "user-2", "user-3" };
            return Ok(usersFake);
        }
    }
}
